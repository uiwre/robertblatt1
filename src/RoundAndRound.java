public class RoundAndRound {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        circle1.radius = 5.1;
        // calculate the remaining attributes for circle1 here
        circle1.diameter = circle1.radius * 2;
        circle1.circumference = 2 * Math.PI * circle1.radius;
        circle1.circumference = Math.round(circle1.circumference * 1000) / 1000.00;
        circle1.area = Math.PI * Math.pow(circle1.radius, 2);
        circle1.area = Math.round(circle1.area * 1000) / 1000.00;

        Circle circle2 = new Circle();
        circle2.radius = 17.5;
        // calculate the remaining attributes for circle2 here
        circle2.diameter = circle2.radius * 2;
        circle2.circumference = 2 * Math.PI * circle2.radius;
        circle2.circumference = Math.round(circle2.circumference * 1000) / 1000.00;
        circle2.area = Math.PI * Math.pow(circle2.radius, 2);
        circle2.area = Math.round(circle2.area * 1000) / 1000.00;

        // Outputs for circle1 and 2
        System.out.println("circle1.radius = " + circle1.radius);
        System.out.println("circle1.diameter = " + circle1.diameter);
        System.out.println("circle1.circumference = " + circle1.circumference);
        System.out.println("circle1.area = " + circle1.area);
        System.out.println("circle2.radius = " + circle2.radius);
        System.out.println("circle2.diameter = " + circle2.diameter);
        System.out.println("circle2.circumference = " + circle2.circumference);
        System.out.println("circle2.area = " + circle2.area);

        Cone cone1 = new Cone(); //Cone2
        cone1.base = circle1;
        cone1.height = 10.3;
        cone1.volume = (cone1.base.area * cone1.height) / 3;
        cone1.volume = Math.round(cone1.volume * 1000) / 1000.0;
        cone1.coat = Math.PI * circle1.radius * Math.sqrt(Math.pow(circle1.radius, 2) + Math.pow(cone1.height, 2));
        cone1.surfaceArea = Math.round((circle1.area + cone1.coat) * 1000) / 1000.00;

        Cone cone2 = new Cone(); //Cone2
        cone2.base = circle2;
        cone2.height = 10.3;
        cone2.volume = (circle2.area * cone2.height) / 3;
        cone2.volume = Math.round(cone2.volume * 1000) / 1000.00;
        cone2.coat = Math.PI * circle2.radius * Math.sqrt(Math.pow(circle2.radius, 2) + Math.pow(cone2.height, 2));
        cone2.surfaceArea = Math.round((circle2.area + cone2.coat) * 1000) / 1000.00;

        System.out.println("cone1.volume = " + cone1.volume);
        System.out.println("cone1.surfaceArea = " + cone1.surfaceArea);
        System.out.println("cone2.volume = " + cone2.volume);
        System.out.println("cone2.surfaceArea = " + cone2.surfaceArea);
    }
}
